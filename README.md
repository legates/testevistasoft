# README #

# VERSÕES #

* PHP 5.6.38
* MySql 10.1.36
* Apache Server 2.4

# CONFIGURAÇÕES #

* Configurar a pasta ROOT do servidor /public
* Apache (Adicionar no .htacess)
*

<IfModule mod_rewrite.c>
    RewriteEngine On

    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} (.+)/$
    RewriteRule ^ %1 [L,R=301]

    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>

