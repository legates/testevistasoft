CREATE TABLE Locador(
    locador_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    locador_nome VARCHAR(200) NOT NULL,
    locador_email VARCHAR(50) NOT NULL,
    locador_telefone VARCHAR(30) NOT NULL,
    locador_dia_repasse VARCHAR(2) NOT NULL
);

CREATE TABLE Locatario(
    locatario_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    locatario_nome VARCHAR(200) NOT NULL,
    locatario_email VARCHAR(50) NOT NULL,
    locatario_telefone VARCHAR(30) NOT NULL
);

CREATE TABLE Imovel(
    imovel_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    imovel_endereco VARCHAR(1000) NOT NULL,
    imovel_proprietario BIGINT NOT NULL,

    FOREIGN KEY (imovel_proprietario) REFERENCES Locador(locador_id)
);

CREATE TABLE Contrato(
    contrato_id BIGINT PRIMARY KEY AUTO_INCREMENT,
    imovel_id BIGINT NOT NULL,
    proprietario_id BIGINT NOT NULL,
    locatario_id BIGINT NOT NULL,
    contrato_data_inicio DATETIME NOT NULL,
    contrato_data_fim DATETIME,
    contrato_taxa_admin VARCHAR(10) NOT NULL,
    contrato_aluguel VARCHAR(10) NOT NULL,
    contrato_iptu VARCHAR(10) NOT NULL,
    contrato_condominio VARCHAR(10) NOT NULL,

    FOREIGN KEY (imovel_id) REFERENCES Imovel(imovel_id),
    FOREIGN KEY (proprietario_id) REFERENCES Locador(locador_id),
    FOREIGN KEY (locatario_id) REFERENCES Locatario(locatario_id)
);

CREATE TABLE Parcela(
    contrato_id BIGINT NOT NULL,
    parcela_num INT NOT NULL,
    parcela_valor VARCHAR(10) NOT NULL,
    parcela_data_venc DATETIME NOT NULL,
    parcela_data_pgto DATETIME,
    parcela_status VARCHAR(1) NOT NULL DEFAULT 1,
    parcela_repasse_valor VARCHAR(10),
    parcela_repasse_status VARCHAR(2) NOT NULL DEFAULT 1,

    FOREIGN KEY (contrato_id) REFERENCES Contrato(contrato_id)
);

    

