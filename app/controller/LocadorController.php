<?php

require_once "Controller.php";
require_once "../app/view/LocadorView.php";

class LocadorController extends Controller{
    public function post($args = []){
        if(empty($args)){
            $this->add();
        } else {
            $this->edit($args);
        }
    }

    public function get($args = []){        
        $query = "SELECT l.locador_id, l.locador_nome, l.locador_email, l.locador_telefone, l.locador_dia_repasse
        FROM Locador l"; 
        
        if(!empty($args)){
            $query .= " WHERE l.locador_id = {$args[0]}";
        }

        $result['locador'] = $this->getFromDB($query);
        
        $view = new LocadorView();
        $view->show($result);
    }

    public function addGET(){    
        $form = '';
        $form .= "<label for='locador_nome'>Nome</label>";
        $form .= "<input required type='text' class='form-control' name='locador_nome'></input><br>";
        $form .= "<label for='locador_email'>E-mail</label>";
        $form .= "<input required type='text' class='form-control' name='locador_email'></input><br>";
        $form .= "<label for='locador_telefone'>Telefone</label>";
        $form .= "<input required type='text' class='form-control' name='locador_telefone'></input><br>";
        $form .= "<label for='locador_dia_repasse'>Dia de Repasse</label>";
        $form .= "<input required type='text' class='form-control' name='locador_dia_repasse'></input><br>";
        $view = new LocadorView();
        $view->formAdd(['add' => $form]);
    }

    public function editGET($args = []){
        $query = "SELECT locador_id, locador_email, locador_telefone FROM Locador WHERE locador_id = {$args[0]}";

        $locador = $this->getFromDB($query);
    
        $form = '';
        $form .= "<label for='locador_email'>E-mail</label>";
        $form .= "<input required type='text' class='form-control' name='locador_email'  value='". $locador[0]['locador_email']."'></input><br>";
        $form .= "<label for='locador_telefone'>Telefone</label>";
        $form .= "<input required type='text' class='form-control' name='locador_telefone'  value='". $locador[0]['locador_telefone']."'></input><br>";

        $view = new LocadorView();
        $view->formEdit(['edit' => $form, "id" => $locador[0]['locador_id']]);
    }

    protected function add(){
        if(!isset($_POST['locador_nome']) 
        || !isset($_POST['locador_email'])
        || !isset($_POST['locador_telefone'])
        || !isset($_POST['locador_dia_repasse'])
        || ($_POST['locador_nome']) == ''
        || ($_POST['locador_email']) == ''
        || ($_POST['locador_telefone']) == ''
        || ($_POST['locador_dia_repasse']) == ''){
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
            die();
        }
        
        $locador_nome = $_POST['locador_nome'];
        $locador_email = $_POST['locador_email'];
        $locador_telefone= $_POST['locador_telefone'];
        $locador_dia_repasse = $_POST['locador_dia_repasse'];

        $query = "INSERT INTO Locador (locador_nome, locador_email, locador_telefone, locador_dia_repasse)
        values ('{$locador_nome}','{$locador_email}','{$locador_telefone}','{$locador_dia_repasse}')";

        if(mysqli_query($this->connection(), $query)){
            header('HTTP/1.1 200 OK');            
            header('Location: /locador');
        } else {
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
        } 
    }

    protected function edit($args = []){
        if(!isset($_POST['locador_email']) 
        || !isset($_POST['locador_telefone']) 
        || ($_POST['locador_email']) == ''
        || ($_POST['locador_telefone']) == ''){
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
            die();
        }
        
        $locadorEmail = $_POST['locador_email'];
        $locadorTelefone = $_POST['locador_telefone'];

        $query = "UPDATE Locador SET locador_email = '{$locadorEmail}', locador_telefone = '{$locadorTelefone}' WHERE locador_id = '{$args[0]}'";

        if(mysqli_query($this->connection(), $query)){
            header('HTTP/1.1 200 OK');            
            header('Location: /locador');
        } else {
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
        } 
    }
}