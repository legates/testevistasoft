<?php

require_once "Controller.php";
require_once "../app/view/ImovelView.php";

class ImovelController extends Controller{
    public function post($args = []){
        if(empty($args)){
            $this->add();
        } else {
            echo "EDIT";
            $this->edit($args);
        }
    }

    public function get($args = []){        
        $query = "SELECT i.imovel_id, i.imovel_endereco, l.locador_nome
        FROM Imovel i
        LEFT JOIN Locador l
        ON i.imovel_proprietario = l.locador_id"; 
        
        if(!empty($args)){
            $query .= " WHERE i.imovel_id = {$args[0]}";
        }

        $result['imovel'] = $this->getFromDB($query);
        
        $view = new ImovelView();
        $view->show($result);
    }

    public function addGET(){
        $query = "SELECT l.locador_id, l.locador_nome
        FROM Locador l"; 

        $locadores = $this->getFromDB($query);
    
        $form = '';
        $form .= "<label for='imovel_endereco'>Endereço</label>";
        $form .= "<input required type='text' class='form-control' name='imovel_endereco'></input><br>";
        $form .= "<label for='imovel_proprietario'>Locador</label>";
        $form .= "<select required class='form-control' name='imovel_proprietario'>";
        foreach($locadores as $locador){
            $form .= "<option value='".$locador['locador_id'] ."'>".$locador['locador_nome']."</option>";
        }
        $form .= "</select><br>";
        $view = new ImovelView();
        $view->formAdd(['add' => $form]);
    }

    public function editGET($args = []){
        $query = "SELECT i.imovel_id, i.imovel_endereco FROM Imovel i WHERE i.imovel_id = {$args[0]}";

        $imovel = $this->getFromDB($query);
    
        $form = '';
        $form .= "<label for='imovel_endereco'>Endereço</label>";
        $form .= "<input required type='text' class='form-control' name='imovel_endereco' value='". $imovel[0]['imovel_endereco']."'</input><br>";
        $view = new ImovelView();
        $view->formEdit(['edit' => $form, "id" => $imovel[0]['imovel_id']]);
    }

    protected function add(){
        if(!isset($_POST['imovel_endereco']) 
        || !isset($_POST['imovel_proprietario']) 
        || ($_POST['imovel_endereco']) == ''
        || ($_POST['imovel_proprietario']) == ''){
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
            die();
        }
        
        $imovelEndereco = $_POST['imovel_endereco'];
        $imovelProprietario = $_POST['imovel_proprietario'];

        $query = "INSERT INTO Imovel (imovel_endereco, imovel_proprietario)
        values ('{$imovelEndereco}','{$imovelProprietario}')";

        if(mysqli_query($this->connection(), $query)){
            header('HTTP/1.1 200 OK');            
            header('Location: /imovel');
        } else {
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
        } 
    }

    protected function edit($args = []){
        if(!isset($_POST['imovel_endereco']) 
        || ($_POST['imovel_endereco']) == ''){
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
            die();
        }
        
        $imovelEndereco = $_POST['imovel_endereco'];

        $query = "UPDATE Imovel SET imovel_endereco = '{$imovelEndereco}' WHERE imovel_id = '{$args[0]}'";

        if(mysqli_query($this->connection(), $query)){
            header('HTTP/1.1 200 OK');            
            header('Location: /imovel');
        } else {
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
        } 
    }
}