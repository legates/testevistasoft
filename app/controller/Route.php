<?php

require_once "../app/Auth.php";
require_once "../app/controller/HomeController.php";
require_once "../app/controller/ContratoController.php";
require_once "../app/controller/LocadorController.php";
require_once "../app/controller/LocatarioController.php";
require_once "../app/controller/ImovelController.php";

class Route
{
    public static function handle()
    {
        $url = explode("/", $_SERVER['REQUEST_URI']);
        $method = $_SERVER["REQUEST_METHOD"];
        $args = [];
        unset($url[0]);

        $controller = ucwords(array_shift($url));

        if (count($url) > 0) {
            foreach ($url as $argument) {
                array_push($args, $argument);
            }
        }

        switch ($controller) {
            case '404':
                echo 'não suportado';
                break;
            case '':
            case 'Index':
                return header('Location: /home');
                break;
            default:
                $controller = "{$controller}Controller";

                $fileController = "..\\app\\controller\\" . $controller . ".php";

                if (file_exists($fileController)) {
                    $controller = new $controller;
                } else {
                    self::errorPage();
                }

                if(!empty($args) && !is_numeric($args[0])){
                    $oldMethod = $method;
                    $method = array_shift($args);
                    $method .= $oldMethod;
                }

                if (is_object($controller)) {
                    $controller->$method($args);
                } else {
                    self::errorPage();
                }
        }
    }

    public static function errorPage()
    {
        return header('Location: /404');
    }
}
