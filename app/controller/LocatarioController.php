<?php

require_once "Controller.php";
require_once "../app/view/LocatarioView.php";

class LocatarioController extends Controller{
    public function post($args = []){
        if(empty($args)){
            $this->add();
        } else {
            $this->edit($args);
        }
    }

    public function get($args = []){        
        $query = "SELECT l.locatario_id, l.locatario_nome, l.locatario_email, l.locatario_telefone
        FROM Locatario l"; 
        
        if(!empty($args)){
            $query .= " WHERE l.locatario_id = {$args[0]}";
        }

        $result['locatario'] = $this->getFromDB($query);
        
        $view = new LocatarioView();
        $view->show($result);
    }

    public function addget(){
        $form = '';
        $form .= "<label for='locatario_nome'>Nome</label>";
        $form .= "<input required type='text' class='form-control' name='locatario_nome'></input><br>";
        $form .= "<label for='locatario_email'>E-mail</label>";
        $form .= "<input required type='text' class='form-control' name='locatario_email'></input><br>";
        $form .= "<label for='locatario_telefone'>Telefone</label>";
        $form .= "<input required type='text' class='form-control' name='locatario_telefone'></input><br>";
        $view = new LocatarioView();
        $view->formAdd(['add' => $form]);
    }

    public function editGET($args = []){
        $query = "SELECT locatario_id, locatario_email, locatario_telefone FROM Locatario WHERE locatario_id = {$args[0]}";

        $locatario = $this->getFromDB($query);
    
        $form = '';
        $form .= "<label for='locatario_email'>E-mail</label>";
        $form .= "<input required type='text' class='form-control' name='locatario_email'  value='". $locatario[0]['locatario_email']."'></input><br>";
        $form .= "<label for='locatario_telefone'>Telefone</label>";
        $form .= "<input required type='text' class='form-control' name='locatario_telefone'  value='". $locatario[0]['locatario_telefone']."'></input><br>";

        $view = new LocatarioView();
        $view->formEdit(['edit' => $form, "id" => $locatario[0]['locatario_id']]);
    }

    protected function add(){
        if(!isset($_POST['locatario_nome']) 
        || !isset($_POST['locatario_email'])
        || !isset($_POST['locatario_telefone'])
        || ($_POST['locatario_nome']) == ''
        || ($_POST['locatario_email']) == ''
        || ($_POST['locatario_telefone']) == ''){
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
            die();
        }
        
        $locatarioNome = $_POST['locatario_nome'];
        $locatarioEmail = $_POST['locatario_email'];
        $locatarioTelefone= $_POST['locatario_telefone'];

        $query = "INSERT INTO Locatario (locatario_nome, locatario_email, locatario_telefone)
        values ('{$locatarioNome}','{$locatarioEmail}','{$locatarioTelefone}')";

        if(mysqli_query($this->connection(), $query)){
            header('HTTP/1.1 200 OK');            
            header('Location: /locatario');
        } else {
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
        } 
    }

    protected function edit($args = []){
        if(!isset($_POST['locatario_email']) 
        || !isset($_POST['locatario_telefone']) 
        || ($_POST['locatario_email']) == ''
        || ($_POST['locatario_telefone']) == ''){
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
            die();
        }
        
        $locatarioEmail = $_POST['locatario_email'];
        $locatarioTelefone = $_POST['locatario_telefone'];

        $query = "UPDATE Locatario SET locatario_email = '{$locatarioEmail}', locatario_telefone = '{$locatarioTelefone}' WHERE locatario_id = '{$args[0]}'";

        if(mysqli_query($this->connection(), $query)){
            header('HTTP/1.1 200 OK');            
            header('Location: /locatario');
        } else {
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
        } 
    }
}