<?php

require_once "Controller.php";
require_once "../app/view/ContratoView.php";

class ContratoController extends Controller
{
    public function post($args = [])
    {
        if (empty($args)) {
            $this->add();
        } else {
            //$this->edit($args);
        }
    }

    public function get($args = [])
    {
        $query = "SELECT c.contrato_id, i.imovel_endereco, l.locador_nome, lo.locatario_nome, c.contrato_data_inicio, c.contrato_data_fim, c.contrato_taxa_admin, c.contrato_aluguel, c.contrato_iptu, c.contrato_condominio 
        FROM Contrato c
        LEFT JOIN Locador l
        ON c.proprietario_id = l.locador_id
        LEFT JOIN Locatario lo
        ON c.proprietario_id = lo.locatario_id
        LEFT JOIN Imovel i
        ON i.imovel_id = c.imovel_id";

        if (!empty($args)) {
            $query .= " WHERE c.contrato_id = {$args[0]}";
        }

        $result['contrato'] = $this->getFromDB($query);

        $view = new ContratoView();
        $view->show($result);
    }

    public function addget()
    {
        $queryLocatarios = "SELECT l.locatario_id, l.locatario_nome FROM Locatario l";
        $locatarios = $this->getFromDB($queryLocatarios);

        $queryImoveis = "SELECT i.imovel_id, i.imovel_endereco, i.imovel_proprietario FROM Imovel i";
        $imoveis = $this->getFromDB($queryImoveis);

        $form = '';
        $form .= "<label for='imovel_id'>Imóvel</label>";
        $form .= "<select required class='form-control' name='imovel_id'>";
        foreach ($imoveis as $imovel) {
            $form .= "<option value='" . $imovel['imovel_id'] . "'>" . $imovel['imovel_endereco'] . "</option>";
        }
        $form .= "</select><br>";

        $form .= "<label for='locatario_id'>Locatário</label>";
        $form .= "<select required class='form-control' name='locatario_id'>";
        foreach ($locatarios as $locatario) {
            $form .= "<option value='" . $locatario['locatario_id'] . "'>" . $locatario['locatario_nome'] . "</option>";
        }
        $form .= "</select><br>";

        $form .= "<label for='contrato_data_inicio'>Data Início</label>";
        $form .= "<input required type='text' class='form-control' name='contrato_data_inicio'></input><br>";

        $form .= "<label for='contrato_taxa_admin'>Taxa Admin %</label>";
        $form .= "<input required type='text' class='form-control' name='contrato_taxa_admin'></input><br>";

        $form .= "<label for='contrato_aluguel'>Aluguel</label>";
        $form .= "<input required type='text' class='form-control' name='contrato_aluguel'></input><br>";

        $form .= "<label for='contrato_iptu'>IPTU</label>";
        $form .= "<input required type='text' class='form-control' name='contrato_iptu'></input><br>";

        $form .= "<label for='contrato_condominio'>Condomínio</label>";
        $form .= "<input type='text' class='form-control' name='contrato_condominio'></input><br>";

        $view = new ContratoView();
        $view->formAdd(['add' => $form]);
    }

    public function parcelaGET($args = [])
    {
        $query = "SELECT p.contrato_id, p.parcela_num, p.parcela_valor, p.parcela_data_venc, p.parcela_data_pgto, p.parcela_status, p.parcela_repasse_valor, p.parcela_repasse_status
        FROM Parcela p WHERE p.contrato_id = {$args[0]}";

        $result['parcela'] = $this->getFromDB($query);

        $view = new ContratoView();
        $view->showParcelas($result);
    }

    public function pagarGET($args = []){
        $query = "SELECT p.contrato_id, p.parcela_num FROM Parcela p WHERE p.contrato_id = {$args[0]} AND p.parcela_num = {$args[1]}";
        $result['parcela'] = $this->getFromDB($query);

        $data = (new \DateTime())->format('Y-m-d H:i:s');

        $query = "UPDATE Parcela p set p.parcela_data_pgto = '{$data}', p.parcela_status = '2' WHERE p.parcela_num =".$result['parcela'][0]['parcela_num']. " AND p.contrato_id = " .$result['parcela'][0]['contrato_id'];

        $conn = $this->connection();
        if (mysqli_query($conn, $query)) {
            header('HTTP/1.1 200 OK');
            header("Location: /contrato/parcela/{$args[0]}");
        } else {
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
        }

    }

    public function repasseGET($args = []){
        $query = "SELECT p.contrato_id, p.parcela_num FROM Parcela p WHERE p.contrato_id = {$args[0]} AND p.parcela_num = {$args[1]}";
        $result['parcela'] = $this->getFromDB($query);

        $query = "UPDATE Parcela p set p.parcela_repasse_status = '2' WHERE p.parcela_num =".$result['parcela'][0]['parcela_num']. " AND p.contrato_id = " .$result['parcela'][0]['contrato_id'];

        $conn = $this->connection();
        if (mysqli_query($conn, $query)) {
            header('HTTP/1.1 200 OK');
            header("Location: /contrato/parcela/{$args[0]}");
        } else {
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
        }

    }

    protected function add()
    {
        if (
            !isset($_POST['imovel_id'])
            || !isset($_POST['locatario_id'])
            || !isset($_POST['contrato_data_inicio'])
            || !isset($_POST['contrato_taxa_admin'])
            || !isset($_POST['contrato_aluguel'])
            || !isset($_POST['contrato_iptu'])
            || ($_POST['imovel_id']) == ''
            || ($_POST['locatario_id']) == ''
            || ($_POST['contrato_data_inicio']) == ''
            || ($_POST['contrato_taxa_admin']) == ''
            || ($_POST['contrato_aluguel']) == ''
            || ($_POST['contrato_iptu']) == ''
        ) {
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
            die();
        }

        $imovelId = $_POST['imovel_id'];
        $locatarioId = $_POST['locatario_id'];
        $contratoDataInicio = $_POST['contrato_data_inicio'];
        $contratoTaxaAdmin = $_POST['contrato_taxa_admin'];
        $contratoAluguel = $_POST['contrato_aluguel'];
        $contratoIptu = $_POST['contrato_iptu'];
        $contratoCondominio = $_POST['contrato_condominio'] != '' ? $_POST['contrato_condominio'] : null;

        $queryLocador = "SELECT i.imovel_id, i.imovel_proprietario FROM Imovel i WHERE i.imovel_id = {$imovelId}";
        $locador = $this->getFromDB($queryLocador);

        $query = "INSERT INTO Contrato (imovel_id, proprietario_id, locatario_id, contrato_data_inicio, contrato_taxa_admin, contrato_aluguel, contrato_iptu "
            . ($contratoCondominio != null ? ', contrato_condominio' : '') . ")
        values ('{$imovelId}'," . $locador[0]['imovel_proprietario'] . ",'{$locatarioId}','{$contratoDataInicio}','{$contratoTaxaAdmin}','{$contratoAluguel}','{$contratoIptu}'" . ($contratoCondominio != null ? ',' . $contratoCondominio : '') . ")";

        $conn = $this->connection();
        if (mysqli_query($conn, $query)) {
            $valorPgto = round(($contratoAluguel + ($contratoIptu / 12) + $contratoCondominio), 2);
            $this->adicionarParcelas($conn->insert_id, $valorPgto, $contratoDataInicio, $contratoTaxaAdmin, $contratoCondominio);

            header('HTTP/1.1 200 OK');
            header('Location: /contrato');
        } else {
            header('HTTP/1.1 500 FAIL');
            header('Location: /home');
        }
    }

    protected function adicionarParcelas($lastId, $valor, $dataContrato, $taxaAdmin, $valorCond = 0)
    {
        $conn = $this->connection();
        $data = strtotime($dataContrato);
        for ($i = 1; $i <= 12; $i++) {
            $parcela = $valor;
            $valorRepasse = 0;
            $data = date("Y-m-d", strtotime("+1 month", $data));
            $split = explode('-', $data);

            $newData = $split[0] . "-" . $split[1] . "-01";

            $data = strtotime($data);

            if ($i == 1) {
                $dateDiff = date_diff(date_create($newData), date_create($dataContrato));
                $diferenca = $dateDiff->format('%a');
                $parcela = round(($parcela / 30) * $diferenca, 2);
                
                $valorRepasse = round((($parcela * (100-$taxaAdmin))/100) - $valorCond, 2);
                $query = "INSERT INTO Parcela (contrato_id, parcela_num, parcela_valor, parcela_data_venc, parcela_repasse_valor)
                VALUES('{$lastId}','{$i}','{$parcela}','{$newData}','{$valorRepasse}')";
            } else {
                $valorRepasse = round((($parcela * (100-$taxaAdmin))/100) - $valorCond, 2);
                $query = "INSERT INTO Parcela (contrato_id, parcela_num, parcela_valor, parcela_data_venc, parcela_repasse_valor)
                VALUES('{$lastId}','{$i}','{$parcela}','{$newData}','{$valorRepasse}')";
            }
            if (!mysqli_query($conn, $query)) {

                header('HTTP/1.1 500 FAIL');
                header('Location: /home');
            } else {
            }
        }
    }
}
