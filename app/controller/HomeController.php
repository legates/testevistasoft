<?php

require_once "../app/view/HomeView.php";

class HomeController{
    public function get(){
        $content = '';

        $view = new HomeView();
        $view->show($content);
    }
}