<?php

require_once "Database.php";

class Controller
{
    public function connection()
    {
        return (new Database())->connect();
    }

    public function getFromDB($query)
    {
        if (!($queryResult = mysqli_query($this->connection(), $query))) {
            header('HTTP/1.1 500 FAIL');
            die();
        }
        $results = [];

        while ($row = mysqli_fetch_assoc($queryResult)) {
            foreach ($row as $key => $result) {
                $item[$key] = $result;
            }
            
            array_push($results, $item);
        }

        return $results;
    }
}
