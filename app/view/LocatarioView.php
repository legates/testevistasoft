<?php

require_once "View.php";

class LocatarioView extends View
{
    public function show($result)
    {
        $table = Html::table($result, "locatario");

        $tags = ['TITLE' => 'Locatários', 'TABLE' => $table];

        $page = file_get_contents("../resources/pages/locatario.php");

        echo $this->preparePage($page, $tags);
    }

    public function formAdd($args = []){
        $tags = ['TITLE' => 'Adicionar Locatário',  'ACTION' => '/locatario', 'FORM' => $args['add']];

        $page = file_get_contents("../resources/pages/form.php");

        echo $this->preparePage($page, $tags);
    }

    public function formEdit($args = []){
        $tags = ['TITLE' => 'Editar Locatário', 'ACTION' => "/locatario/{$args['id']}", 'FORM' => $args['edit']];

        $page = file_get_contents("../resources/pages/form.php");

        echo $this->preparePage($page, $tags);
    }
}
