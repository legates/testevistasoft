<?php

require_once "View.php";

class HomeView extends View{
    public function show($content){
        $tags = ['TITLE' => 'Início'];

        $page = file_get_contents("../resources/pages/home.php");

        echo $this->preparePage($page, $tags);
    }
}