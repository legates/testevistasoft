<?php

require_once "View.php";

class ImovelView extends View
{
    public function show($result)
    {        
        $table = Html::table($result, "imovel");

        $tags = ['TITLE' => 'Imóveis', 'TABLE' => $table];

        $page = file_get_contents("../resources/pages/imovel.php");

        echo $this->preparePage($page, $tags);
    }

    public function formAdd($args = []){
        $tags = ['TITLE' => 'Adicionar Imóvel',  'ACTION' => '/imovel', 'FORM' => $args['add']];

        $page = file_get_contents("../resources/pages/form.php");

        echo $this->preparePage($page, $tags);
    }

    public function formEdit($args = []){
        $tags = ['TITLE' => 'Editar Imóvel', 'ACTION' => "/imovel/{$args['id']}", 'FORM' => $args['edit']];

        $page = file_get_contents("../resources/pages/form.php");

        echo $this->preparePage($page, $tags);
    }
}
