<?php

require_once "View.php";

class ContratoView extends View
{
    public function show($result)
    {
        $table = Html::table($result, "contrato", "parcela", false);

        $tags = ['TITLE' => 'Contratos', 'TABLE' => $table];

        $page = file_get_contents("../resources/pages/contrato.php");

        echo $this->preparePage($page, $tags);
    }

    public function showParcelas($result)
    {
        $table = '';

        foreach ($result['parcela'] as $row) {
            $table .= "<tr>";
            foreach ($row as $key => $field) {
                if ($key == 'parcela_status' || $key == "parcela_repasse_status") {
                    if ($field == 1) {
                        $table .= "<td>Em aberto<td>";
                    } else {
                        $table .= "<td>Pago<td>";
                    }
                } else {
                    $table .= "<td>" .  $field . "<td>";
                }
            }
            if ($row['parcela_status'] == 1) {
                $table .= "<td><a href='/contrato/pagar/" . $row['contrato_id'] . "/" . $row['parcela_num'] . "'><div class='btn btn-primary'>Pagar</div></a>";
            } else {
                $table .= "<td></td>";
            }
            if ($row['parcela_repasse_status'] == 1) {
                $table .= "<td><a href='/contrato/repasse/" . $row['contrato_id'] . "/" . $row['parcela_num'] . "'><div class='btn btn-primary'>Repassar</div></a>";
            }
            $table .=  "</tr>";
        }

        $tags = ['TITLE' => 'Parcelas', 'TABLE' => $table];

        $page = file_get_contents("../resources/pages/parcela.php");

        echo $this->preparePage($page, $tags);
    }

    public function formAdd($args = [])
    {
        $tags = ['TITLE' => 'Adicionar Contrato',  'ACTION' => '/contrato', 'FORM' => $args['add']];

        $page = file_get_contents("../resources/pages/form.php");

        echo $this->preparePage($page, $tags);
    }

    public function formEdit($args = [])
    {
        $tags = ['TITLE' => 'Editar Contrato', 'ACTION' => "/contrato/{$args['id']}", 'FORM' => $args['edit']];

        $page = file_get_contents("../resources/pages/form.php");

        echo $this->preparePage($page, $tags);
    }
}
