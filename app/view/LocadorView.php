<?php

require_once "View.php";

class LocadorView extends View
{
    public function show($result)
    {        
        $table = Html::table($result, "locador");

        $tags = ['TITLE' => 'Locadores', 'TABLE' => $table];

        $page = file_get_contents("../resources/pages/locador.php");

        echo $this->preparePage($page, $tags);
    }

    public function formAdd($args = []){
        $tags = ['TITLE' => 'Adicionar Locador',  'ACTION' => '/locador', 'FORM' => $args['add']];

        $page = file_get_contents("../resources/pages/form.php");

        echo $this->preparePage($page, $tags);
    }

    public function formEdit($args = []){
        $tags = ['TITLE' => 'Editar Locador', 'ACTION' => "/locador/{$args['id']}", 'FORM' => $args['edit']];

        $page = file_get_contents("../resources/pages/form.php");

        echo $this->preparePage($page, $tags);
    }
}
