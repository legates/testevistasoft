<?php

require_once "Html.php";

class View{
    public function preparePage($page, $tags){        

        $basePage = file_get_contents("../resources/pages/base.php");

        $basePage = str_replace("[!!TITLE!!]", $tags['TITLE'], $basePage);
        
        $prepared = str_replace("[!!CONTENT!!]", $page, $basePage);
        
        foreach($tags as $key => $tag){
            $prepared = str_replace("[!!${key}!!]", $tag, $prepared);

        }       

        return $prepared;
    }
}