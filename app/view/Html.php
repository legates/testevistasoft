<?php

class Html
{
    public static function table($result, $entity, $extraBtn = null, $edit = true)
    {
        $table = '';

        foreach ($result[$entity] as $row) {
            $table .= "<tr>";
            foreach ($row as $field) {
                $table .= "<td>" .  $field . "<td>";
            }
            $entityID = $entity . "_id";
            if($edit){
                $table .= "<td><a href='/{$entity}/edit/" . $row[$entityID] . "'><div class='btn btn-primary'>Editar</div></a>";
            }
            if ($extraBtn != null) {
                $table .= "<td><a href='/{$entity}/{$extraBtn}/" . $row[$entityID] . "'><div class='btn btn-primary'>" . ucfirst($extraBtn) . "</div></a>";
            }
            $table .=  "</tr>";
        }

        return $table;
    }
}
