<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="http://localhost/css/bootstrap.min.css" rel="stylesheet">
  <title>[!!TITLE!!]</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">VistaSoft</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="/">Início</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/locador">Locadores</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/locatario">Locatários</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/imovel">Imóveis</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/contrato">Contratos</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="container">
    <h3>[!!TITLE!!]</h3>
    <div class="row">
      [!!CONTENT!!]
    </div>
  </div>
  <script src="../js/bootstrap.bundle.min.js"></script>
</body>

</html>